﻿using BaseMobile.Views;
using FFImageLoading.Svg.Forms;
using System;
using System.Linq;
using System.Reflection;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BaseMobile
{
    public partial class App : Application
    {
        public App(Object platformContext)
        {
            InitializeComponent();

            Assembly assembly = platformContext.GetType().Assembly;
            string[] resourceNames = assembly.GetManifestResourceNames();

            string centerLogoResourceName = 
                resourceNames.SingleOrDefault(x => x?.ToLower()?.Contains("Images.CenterLogo.svg".ToLower()) == true);

            string bottomLogoResourceName
                = resourceNames.SingleOrDefault(x => x?.ToLower()?.Contains("Images.BottomLogo.svg".ToLower()) == true);

            string loaderResourceName
                = resourceNames.SingleOrDefault(x => x?.ToLower()?.Contains("Images.Loader.svg".ToLower()) == true);

            MainPage = new Loading(
                SvgImageSource.FromStream(() => assembly.GetManifestResourceStream(centerLogoResourceName)),
                SvgImageSource.FromStream(() => assembly.GetManifestResourceStream(loaderResourceName)),
                SvgImageSource.FromStream(() => assembly.GetManifestResourceStream(bottomLogoResourceName)));
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
