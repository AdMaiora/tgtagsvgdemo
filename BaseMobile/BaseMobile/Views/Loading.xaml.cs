﻿using FFImageLoading.Svg.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BaseMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Loading : ContentPage
	{
        private object _platformContext;

        public Loading(
            SvgImageSource centerLogo, 
            SvgImageSource rotatingLogo, 
            SvgImageSource bottomLogo)
        {
            InitializeComponent();

            /*
             * https://github.com/luberda-molinet/FFImageLoading/issues/1129
             * Thre's a delay loading SVGs due to the usage of FFImageLoading library
             * 
             */

            this.CenterLogoImage.Source = centerLogo;
            this.RotatingImage.Source = rotatingLogo;
            this.BottomLogoImage.WidthRequest = DeviceDisplay.MainDisplayInfo.Width;
            this.BottomLogoImage.Source = bottomLogo;
        }

        protected override void LayoutChildren(double x, double y, double width, double height)
        {
            base.LayoutChildren(x, y, width, height);

            this.RotatingImage.WidthRequest = width * .66;
            this.RotatingImage.HeightRequest = width * .66;

            this.CenterLogoImage.WidthRequest = width * .33;
            this.CenterLogoImage.HeightRequest = width * .33;            
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            new Animation(v => this.RotatingImage.Rotation = v, 0, 360)
                .Commit(this.RotatingImage, "RotatingLoader", 16, 5000, null, (v, c) => this.RotatingImage.Rotation = 0, () => true);

            // Simulate percentage progress
            new Animation(v => this.ProgressLabel.Text = $"Uploading Inspection: {((int)v).ToString()}%", 0, 100)
                .Commit(this.ProgressLabel, "ProgressCounter", 1000, 5000, null, (v, c) => this.ProgressLabel.Text = "0%", () => true);


            await this.FadeInPanel.FadeTo(0, 1500, Easing.Linear);
        }
    }
}
 